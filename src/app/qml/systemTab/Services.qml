/*
  This file is part of ut-tweak-tool
  Copyright (C) 2016 Stefano Verzegnassi <verzegnassi.stefano@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import TweakTool 1.0

import "../components/ListItems" as ListItems
import "../components"

Page {
    id: servicesPage

    header: PageHeader {
        title: i18n.tr("Services")
        flickable: scrollView.flickableItem
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent

        Column {
            width: scrollView.width

            ListItems.SectionDivider {
                text: i18n.tr("Restart services")
            }

            ListItems.Control {
                title.text: i18n.tr("Restart Unity 8")
                subtitle.text: i18n.tr(
                    "Close all running apps and restart the home screen."
                )
                subtitle.maximumLineCount: -1

                onClicked: {
                    var popup = PopupUtils.open(confirmDialog);
                    popup.accepted.connect(function() {
                        Process.launch("restart unity8");
                    })
                }
            }
        }
    }

    Component {
        id: confirmDialog
        Dialog {
            id: confirmDialogue

            signal accepted;

            title: i18n.tr("Restart Unity 8")
            text: i18n.tr("Do you want to restart Unity 8 now?") +
                "<br />" + i18n.tr("All currently open apps will be closed.") +
                "<br />" + i18n.tr("Save all your work before continuing!")

            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(confirmDialogue);
            }

            Button{
                text: i18n.tr("Restart Unity 8")
                color: theme.palette.normal.negative
                onClicked: {
                    confirmDialogue.accepted();
                    PopupUtils.close(confirmDialogue);
                }
            }
        }
    }
}
