/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import TweakTool 1.0
import QtQuick.Layouts 1.1

import "../components"
import "../components/ListItems" as ListItems

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("Make image writable")
        flickable: view.flickableItem
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.Warning {
                iconName: "security-alert"
                text: i18n.tr(
                    "Be very careful when editing the filesystem image. There is a high risk of breaking OTA updates."
                )
            }

            ListItems.SectionDivider { text: i18n.tr("Available settings") }

            ListItems.OptionSelector {
                id: selector
                model: [
                    i18n.tr("[Read-Only] Default setting"),
                    i18n.tr("[Read-Write] Temporary (until reboot)"),
                    i18n.tr("[Read-Write] Permanent (persists after reboot)")
                ]

                function displayRoRwSetting() {
                    var outputStatus = Process.launch(
                        '/bin/sh -c "mount | grep \' on / \' | cut -d\\( -f2 | cut -d, -f1"'
                    );
                    var rwActive = outputStatus.indexOf("rw") > -1;

                    if (!writable_image.exists && !rwActive) {
                        selectedIndex = 0;
                    } else if (!writable_image.exists && rwActive) {
                        selectedIndex = 1;
                    } else {
                        selectedIndex = 2;
                    }
                }

                Component.onCompleted: {
                    displayRoRwSetting();
                }
                onSelectedIndexChanged:  {
                    var outputRO = "";
                    var errorRO = false;
                    var outputRW = "";

                    if (selectedIndex == 0) {
                        outputRO = Process.launch(
                            '/bin/sh -c "echo ' + pamLoader.item.password + ' | sudo -S mount -o ro,remount /',
                            true
                        );
                        errorRO = outputRO.indexOf("mount: / is busy") > -1;
                        if (errorRO) outputLabel.text += "<br />" + outputRO;
                    } else {
                        outputRW = Process.launch(
                            '/bin/sh -c "echo ' + pamLoader.item.password + ' | sudo -S mount -o rw,remount /',
                            true
                        );
                    }

                    if (selectedIndex == 2) {
                        writable_image.touch();
                    } else {
                        if (writable_image.exists && !errorRO) writable_image.rm();
                    }

                    // Will only be different from the selection if there were errors
                    if (errorRO) displayRoRwSetting();
                }
            }

            ListItems.SectionDivider {
                text: i18n.tr("Output")
            }

            ListItem {
                height: outputLabel.height + outputLabel.anchors.topMargin * 2
                divider.visible: false

                Label {
                    id: outputLabel
                    text: i18n.tr("Any errors will be displayed below.") + "<br />"
                    font.family: "Ubuntu Mono"
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        margins: units.gu(1)
                    }
                }
            }
        }
    }

    SystemFile {
        id: writable_image
        filename: "/userdata/.writable_image"
        onPasswordRequested: providePassword(pam.password)
    }
}
