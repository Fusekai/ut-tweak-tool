# Ut-tweak-tool

## Building
Install [clickable](https://gitlab.com/clickable/clickable).

Now on you can build the app with:

```bash
$ clickable
```
    
## Download
Downloads of the current (and previous) versions are available from the Tags page:

* https://gitlab.com/myii/ut-tweak-tool/tags

Or on OpenStore:

* https://open.uappexplorer.com/app/ut-tweak-tool.sverzegnassi